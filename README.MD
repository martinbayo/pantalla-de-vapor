Pantalla de Vapor
=================

<iframe title="cryptojardin" src="https://fediverse.tv/videos/embed/c12920c6-1652-434a-bceb-25fbcaae9f65" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

[Video de muestra](https://fediverse.tv/w/pRr9dC76xNAwZ7RNdBT3Dk)


<video src='https://fediverse.tv/w/pRr9dC76xNAwZ7RNdBT3Dk' width=180/>

<h2>Pantalla de Vapor </h2>

El siguiente proyecto surge de la invitación de[ Laura Benech](https://laurabenech.net " Laura Benech")  a colaborar en el armado de una pantalla para el proyecto Criptojardín que presentó en Apoyo a la producción. [Arte Contemporáneo MediaLab CCEBA 2022.](https://www.cceba.org.ar/medialab/proyectos-seleccionados-y-menciones "Arte Contemporáneo MediaLab CCEBA 2022.")  

El funcionamentio se trató de simplificar lo mas posible, cuenta con un atomizador ultrasonico de 10 cabezales encerrado en un deposito y 2 corrientes de aire a cada costado de la salida del deposito con el fin de armar una cortina de vapor, como consume agua se rellena el deposito con una bomba de agua. 
Luego de investigar proyectos que funcionaron, decidimos tomar como base de diseño a[ Hoverlay II ](https://github.com/makertum/Hoverlay-II  " Hoverlay II ") - el cual nos sirvio de mucha Ayuda - Gracias  Moritz !


<h2>Agradecimientos </h2>
a Laura por meterme en este embrollo, complicado y muy entretenido :) , a las observaciones y comentarios muy utiles de Laura Godoy, Eduardo Miró, Gustavo Courault y Marcelo Scagnetti. Tambien a Santiago , Pedro y Carolina que siempre bancan

<h2>Los materiales que utilizamos fueron:</h2>

- 10 cooler - Marca Sunon  - modelo MF80251v1- de 12 V
- 1 atomizador ultrasonico  Gdrasuya10 
- 2 reguladores de tension DC DC - lm2596 (para los dos cooler de los costados) y xl4015 (para los 8 cooler del flujo laminar)
- 1 fuente de pc reciclada
- 1 Bomba Sumergible Sobo Wp-3400 H/1.2mts
- Manguera de 10mm
- 1 varilla roscada de 6mm y 4 tuercas
- 40 tornillos para cooler o similar
- 1 "pecera" de acrilico base = 120 mm interior, angulo = 79 °, alto = 104 mm interior, tamaño arriba = 80 mm interior , com paredes de 4mm y 6 en los extremos 
- 2 planchas de policarbonato de 4mm - 43cm x 21 cm

<h2>Se realizaron varios diseños en Freecad e Impresiones 3D en una ender 3-se deben imprimir: </h2>

- Interior_con_celda
- Interior_con_celda_manguera
- cono_cooler x 2
- Panal x 4
- Plancha cooler x 4
- tapa_aire_costado x 2
- Tapa_aire_costado_espejo x 2
- Ganchitos x 10 - para armado provisorio antes de pegar todo
- ganchitos2 x 10 - para armado provisorio antes de pegar todo
- Manguera - para desague de agua (va en la "pecera")
- Regular - para asegurarnos que no entra mas agua de la necesitamos
- cajita para lm2596 - https://www.thingiverse.com/thing:3164882/files
- cajita para xl4015 https://www.thingiverse.com/thing:5419327/files

<h2>Licencia</h2>

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licencia Creative Commons Atribución-CompartirIgual 4.0 Internacional</a>.

